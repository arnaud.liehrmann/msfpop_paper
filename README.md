FOREWORD: 

(1) YOU SHOULD MAKE SURE THAT YOU HAVE INSTALLED THE FOLLOWING PACKAGES 
    
  install.packages("devtools")
  
  devtools::install_git(url = "https://forgemia.inra.fr/arnaud.liehrmann/psifpop")
  
  install.packages("parallel")
  
  install.packages("ggrepel")
  
  install.packages("ggplot2")
  
  install.packages("stringr")
  
  install.packages("data.table")
  
  install.packages("ggpubr")
  
  install.packages("fpopw")
  
  install.packages("changepoint")
  
  install.packages("latex2exp")
  
  
THIS GIT REPOSITORY CONTAINS:

(1) R SCRIPT TO COMPARE THE RUNTIME OF MS.PELT AND MS.FPOP

    => You should look at runtime_cmp_gr.R and runtime_cmp_al.R in the R repository.

(2) R SCRIPT TO COMPARE THE RUNTIME OF SAMPLING STRATEGIES OF FUTURE CANDIDATES IN MS.FPOP

    => You should look at runtime_cmp_al.R in the R repository.
    

(3) R SCRIPT TO CALIBRATE CONSTANTS OF THE MULTISCALE PENALTY 

    => You should look at calibrateMsFPOP.R in the R repository.
    

(4) R SCRIPT TO COMPARE MS.FPOP AND FPOP ON STEP SIMULATIONS

    => You should look at step_simu.R in the R repository.
    
(5) R SCRIPT TO COMPARE MS.FPOP AND FPOP ON HAT SIMULATIONS

    => You should look at hat_simu.R in the R repository.

(6) R SCRIPT TO COMPARE MOSUM, MS.FPOP AND FPOP ON AN EXTENDED RANGE OF SIMULATIONS
    
    => You should look at somesegsimu_figures.R in the R repository.
